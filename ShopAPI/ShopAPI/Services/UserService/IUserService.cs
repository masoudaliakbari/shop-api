﻿using ShopAPI.DTO;
using ShopAPI.DTO.User;

namespace ShopAPI.Services.UserService
{
    public interface IUserService
    {
        UserReadDTO Authenticate(string username, string password);
        UserReadDTO Add(NewUserDTO newUser);
        void CheckRolesData();
    }
}
