﻿using AutoMapper;
using ShopAPI.DTO;
using ShopAPI.DTO.User;
using ShopAPI.Helper.Security;
using ShopAPI.Models;

namespace ShopAPI.Services.UserService
{
    public class UserService : IUserService
    {
        private readonly ShopContext db;
        private readonly IMapper mapper;

        public UserService(ShopContext db, IMapper mapper)
        {
            this.db = db;
            this.mapper = mapper;
        }

        public UserReadDTO Add(NewUserDTO newUser)
        {
            string encryptedPassword = SecurityUtility.Encrypt(newUser.Password); // Encrypted password algorithm
            User user = new User()
            {
                Enable = true,
                UserName = newUser.UserName,
                UserPassword = encryptedPassword,
                RoleId = newUser.RoleId,
                FirstName = newUser.FirstName,
                LastName = newUser.LastName,
            };
            db.Users.Add(user);
            db.SaveChanges();
            return mapper.Map<UserReadDTO>(user);
        }

        public UserReadDTO Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password)) return null;

            string encryptedPassword = SecurityUtility.Encrypt(password); // Encrypted password algorithm

            // Select User With Username and Password from database...
            var user = GetUser(username, encryptedPassword);

            return user;
        }

        public void CheckRolesData()
        {
            var roles = db.Roles.ToList();

            // fill table with based data if table is empty, this run just for first run.
            if(roles.Count() == 0)
            {
                db.Roles.Add(new Role
                {
                    Id = Convert.ToInt32(Helper.UserRole.Normal),
                    Name = "Normal"
                });
                db.Roles.Add(new Role
                {
                    Id = Convert.ToInt32(Helper.UserRole.Admin),
                    Name = "Admin"
                });

                db.SaveChanges();
            }
        }

        private UserReadDTO GetUser(string username, string encryptedPassword)
        {
            var dbUser = db.Users.FirstOrDefault(u=>u.UserName == username && u.UserPassword == encryptedPassword);
            UserReadDTO result = mapper.Map<UserReadDTO>(dbUser);
            return result;
        }
    }
}