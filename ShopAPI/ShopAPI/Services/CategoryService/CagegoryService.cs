﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ShopAPI.DTO.Category;
using ShopAPI.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace ShopAPI.Services.CategoryService
{
    public class CagegoryService : ICategoryService
    {
        private readonly ShopContext db;
        private readonly IMapper mapper;

        public CagegoryService(IMapper mapper, ShopContext context)
        {
            db = context;
            this.mapper = mapper;
        }

        public CategoryReadDTO Add(CategoryAddDTO category)
        {
            Category newCategory = new Category
            {
                Id = category.Id,
                Name = category.Name
            };

            db.Categories.Add(newCategory);
            db.SaveChanges();

            return mapper.Map<CategoryReadDTO>(newCategory);
        }

        public CategoryPage GetCategories(int page = 1, int pageSize = 10, string filter = "")
        {
            var categories = db.Categories.AsQueryable();
            if(!string.IsNullOrEmpty(filter) )
            {
                categories = categories.Where(item => item.Name.Contains(filter));
            }
            var totalCount = categories.Count();
            var totalPages = (int)Math.Ceiling((double)totalCount / pageSize);
            categories = categories.Skip((page - 1) * pageSize).Take(pageSize);

            var result = Array.ConvertAll(categories.ToArray(), c => mapper.Map<CategoryReadDTO>(c));

            var pageResult = new CategoryPage
            {
                TotalCount = totalCount,
                TotalPages = totalPages,
                CurrentPage = page,
                PageSize = pageSize,
                Categories = result
            };

            return pageResult;
        }

        public CategoryReadDTO GetCategory(int id)
        {
            Category? category = db.Categories.FirstOrDefault(c => c.Id == id);
            CategoryReadDTO result = mapper.Map<CategoryReadDTO>(category);
            return result;
        }

        public void Update(CategoryReadDTO category)
        {
            Category? oldCategory = db.Categories.FirstOrDefault(c => c.Id == category.Id);
            oldCategory.Name = category.Name;
            db.SaveChanges();
        }
    }
}
