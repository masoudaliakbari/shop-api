﻿using ShopAPI.DTO.Category;

namespace ShopAPI.Services.CategoryService
{
    public interface ICategoryService
    {
        CategoryPage GetCategories(int page = 1, int pageSize = 10, string filter = "");

        CategoryReadDTO Add(CategoryAddDTO category);

        CategoryReadDTO GetCategory(int id);

        void Update(CategoryReadDTO category);
    }
}
