﻿using ShopAPI.DTO;
using ShopAPI.DTO.Product;

namespace ShopAPI.Services.ProductService
{
    public interface IProductService
    {
        ProductPage GetProducts(int page = 1, int pageSize = 10, string filter = "");

        ProductReadDTO Add(ProductAddDTO product);

        ProductReadDTO GetProduct(int id);

        void Update(ProductReadDTO product);
    }
}