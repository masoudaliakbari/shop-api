﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ShopAPI.DTO;
using ShopAPI.DTO.Product;
using ShopAPI.Models;

namespace ShopAPI.Services.ProductService
{
    public class ProductService : IProductService
    {
        private readonly ShopContext db;
        private readonly IMapper mapper;

        public ProductService(ShopContext context, IMapper mapper)
        {
            db = context;
            this.mapper = mapper;
        }

        public ProductReadDTO Add(ProductAddDTO product)
        {
            Product newProduct = new Product
            {
                Id = product.Id,
                Name = product.Name,
                CategoryId = product.CategoryId
            };

            db.Products.Add(newProduct);
            db.SaveChanges();

            return mapper.Map<ProductReadDTO>(newProduct);
        }

        public ProductReadDTO GetProduct(int id)
        {
            Product? product = db.Products.FirstOrDefault(p => p.Id == id);
            ProductReadDTO result = mapper.Map<ProductReadDTO>(product);
            return result;
        }

        public void Update(ProductReadDTO product)
        {
            Product? oldProduct = db.Products.FirstOrDefault(p => p.Id == product.Id);
            oldProduct.Name = product.Name;
            oldProduct.CategoryId = product.CategoryId;
            db.SaveChanges();
        }

        public ProductPage GetProducts(int page = 1, int pageSize = 10, string filter = "")
        {
            var products = db.Products
                .Include(p=> p.Category)
                .AsQueryable();

            if (!string.IsNullOrEmpty(filter))
            {
                products = products.Where(item => item.Name.Contains(filter));
            }

            var totalCount = products.Count();
            var totalPages = (int)Math.Ceiling((double)totalCount / pageSize);
            products = products.Skip((page - 1) * pageSize).Take(pageSize);
            var result = Array.ConvertAll(products.ToArray(), p => mapper.Map<ProductReadDTO>(p));

            var pageResult = new ProductPage
            {
                TotalCount = totalCount,
                TotalPages = totalPages,
                CurrentPage = page,
                PageSize = pageSize,
                Products = result
            };

            return pageResult;
        }
    }
}
