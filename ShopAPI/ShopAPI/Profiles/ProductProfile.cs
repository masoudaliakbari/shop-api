﻿using AutoMapper;
using ShopAPI.DTO.Product;
using ShopAPI.Models;

namespace ShopAPI.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Product, ProductReadDTO>();
        }
    }
}
