﻿using AutoMapper;
using ShopAPI.DTO.Category;
using ShopAPI.Models;

namespace ShopAPI.Profiles
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<Category, CategoryReadDTO>();
        }
    }
}
