﻿using AutoMapper;
using ShopAPI.DTO;
using ShopAPI.DTO.User;
using ShopAPI.Models;

namespace ShopAPI.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, NewUserDTO>();
            CreateMap<User, UserReadDTO>();
        }
    }
}
