﻿namespace ShopAPI.Helper
{
    public enum UserRoles
    {
        NormalUser = 1,
        AdminUser = 2
    }

    public class UserRole
    {
        public const string Normal = "1";
        public const string Admin = "2";
    }
}
