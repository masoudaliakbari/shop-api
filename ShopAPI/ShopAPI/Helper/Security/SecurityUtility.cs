﻿using Microsoft.IdentityModel.Tokens;
using ShopAPI.DTO;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace ShopAPI.Helper.Security
{
    public class SecurityUtility : ISecurityUtility
    {
        private readonly IConfiguration configuration;
        private const int keysize = 256;
        private static readonly byte[] initVectorBytes = Encoding.ASCII.GetBytes("tu89geji340t89u2");

        public SecurityUtility(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public TokenInfo BuildToken(UserReadDTO user)
        {
            string tokenKey = configuration["JWT:key"]!;
            string issuer = configuration["JWT:issuer"]!;
            string audience = configuration["JWT:audience"]!;

            var claims = new[] {
                new Claim("FullName", user.FirstName+" "+user.LastName),
                new Claim("ID", user.Id.ToString()),
                new Claim(ClaimTypes.Role, user.RoleId.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            DateTime expiredToken = DateTime.Now.AddHours(12); // expired after 12 hour;
            var token = new JwtSecurityToken(issuer,
                audience,
                claims,
                expires: expiredToken,
                signingCredentials: creds
                );

            string resultToken = new JwtSecurityTokenHandler().WriteToken(token);

            return new TokenInfo
            {
                TokenString = resultToken,
                Expiration = token.ValidTo
            };
        }

        internal static string Encrypt(string plainText)
        {
            string passPhrase = "MyKey5522336";
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                byte[] cipherTextBytes = memoryStream.ToArray();
                                string result = Convert.ToBase64String(cipherTextBytes);
                                return result;
                            }
                        }
                    }
                }
            }
        }
    }

    public class TokenInfo
    {
        public string TokenString { get; set; }
        public DateTime Expiration { get; set; }
    }
}
