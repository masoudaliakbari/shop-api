﻿using ShopAPI.DTO;

namespace ShopAPI.Helper.Security
{
    public interface ISecurityUtility
    {
        TokenInfo BuildToken(UserReadDTO user);
    }
}
