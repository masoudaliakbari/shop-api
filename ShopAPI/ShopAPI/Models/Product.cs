﻿using System;
using System.Collections.Generic;

namespace ShopAPI.Models;

public partial class Product
{
    public int Id { get; set; }

    public int CategoryId { get; set; }

    public string Name { get; set; } = null!;

    public DateTime CreateDate { get; set; }

    public bool Deleted { get; set; }

    public virtual Category Category { get; set; } = null!;
}
