﻿using System;
using System.Collections.Generic;

namespace ShopAPI.Models;

public partial class User
{
    public int Id { get; set; }

    public int RoleId { get; set; }

    public string FirstName { get; set; } = null!;

    public string? LastName { get; set; }

    public string UserName { get; set; } = null!;

    public string UserPassword { get; set; } = null!;

    public bool? Enable { get; set; }

    public virtual Role Role { get; set; } = null!;
}
