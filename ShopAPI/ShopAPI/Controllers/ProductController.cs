﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShopAPI.DTO.Product;
using ShopAPI.Helper;
using ShopAPI.Models;
using ShopAPI.Services.ProductService;

namespace ShopAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        IProductService productService;
        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        /// <summary>
        /// Get All Product with pagination and filter options
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet("Products")]
        public ActionResult GetAll([FromQuery] int page = 1, [FromQuery] int pageSize = 10, [FromQuery] string filter = "")
        {
            var products = productService.GetProducts(page, pageSize, filter);

            return Ok(products);
        }

        /// <summary>
        /// Add new Product or update if it does exist
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = UserRole.Admin)]
        public ActionResult Add([FromBody] ProductAddDTO product)
        {
            return AddOrUpdate(product);
        }

        /// <summary>
        /// Add Or Update Product, Add it if it doesn't exist else Update it.
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        private ActionResult AddOrUpdate(ProductAddDTO product)
        {
            ProductReadDTO oldProduct = productService.GetProduct(product.Id);

            // Add...
            if (oldProduct == null)
            {
                ProductReadDTO createdProduct = productService.Add(product);
                return Created(createdProduct.Name, createdProduct);
            }

            // Update...
            oldProduct.Name = product.Name;
            oldProduct.CategoryId = product.CategoryId;
            productService.Update(oldProduct);
            return Ok(oldProduct);
        }

        /// <summary>
        /// Get Product base on ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult Get([FromRoute] int id)
        {
            var product = productService.GetProduct(id);

            if (product != null) return Ok(product);

            return BadRequest($"id: {id} does not exist.");
        }

        /// <summary>
        /// Add new Product or update if it does exist
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize(Roles = UserRole.Admin)]
        public ActionResult Update([FromBody] ProductAddDTO product)
        {
            return AddOrUpdate(product);
        }
    }
}
