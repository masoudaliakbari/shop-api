﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using ShopAPI.DTO;
using ShopAPI.DTO.User;
using ShopAPI.Helper.Security;
using ShopAPI.Services.UserService;

namespace ShopAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly ISecurityUtility securityUtility;

        public UserController(IUserService userService, ISecurityUtility securityUtility)
        {
            this.userService = userService;
            this.securityUtility = securityUtility;
        }

        /// <summary>
        /// Login user with username and password
        /// </summary>
        /// <param name="authDTO"></param>
        /// <returns></returns>
        [HttpPost("Authenticate")]
        public IActionResult Login([FromBody] AuthDTO authDTO)
        {
            UserReadDTO authUser = userService.Authenticate(authDTO.Username, authDTO.Password);
            if (authUser == null) // User not found.
            {
                return Unauthorized();
            }

            TokenInfo token = securityUtility.BuildToken(authUser);

            return Ok(new
            {
                Token = token.TokenString,
                FullName = authUser.FirstName + " " + authUser.LastName,
                expiration = token.Expiration
            });
        }

        /// <summary>
        /// Just For Test and Add new User
        /// </summary>
        /// <param name="newUser"></param>
        /// <returns></returns>
        [HttpPost("Add")]
        public IActionResult AddUser([FromBody] NewUserDTO newUser)
        {
            // for based data
            userService.CheckRolesData();

            var user = userService.Add(newUser);
            return Created(user.UserName, user);
        }
    }
}
