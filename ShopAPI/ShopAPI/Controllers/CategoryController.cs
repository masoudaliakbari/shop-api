﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShopAPI.DTO.Category;
using ShopAPI.Helper;
using ShopAPI.Services.CategoryService;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace ShopAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            this.categoryService = categoryService;
        }
        /// <summary>
        /// Get All Categories with pagination and filter options
        /// </summary>
        /// <param name="page">page number</param>
        /// <param name="pageSize">page items count</param>
        /// <param name="filter">Category Name filter, leave empty for ignore it.</param>
        /// <returns></returns>
        [HttpGet("Categories")]
        public ActionResult GetAll([FromQuery] int page = 1, [FromQuery] int pageSize = 10, [FromQuery] string filter = "")
        {
            var categories = categoryService.GetCategories(page, pageSize, filter);

            return Ok(categories);
        }

        /// <summary>
        /// Add new Category or update if it does exist
        /// </summary>
        /// <param name="category">Category name</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = UserRole.Admin)]
        public ActionResult Add([FromBody] CategoryAddDTO category)
        {
            return AddOrUpdate(category);
        }

        /// <summary>
        /// Get Category based on ID
        /// </summary>
        /// <param name="id">Category ID</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult Get([FromRoute] int id)
        {
            var category = categoryService.GetCategory(id);

            if (category != null) return Ok(category);

            return BadRequest($"id: {id} does not exist.");
        }

        /// <summary>
        /// Update Category or Add if it doesn't exist
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize(Roles = UserRole.Admin)]
        public ActionResult Update([FromBody] CategoryAddDTO category)
        {
            return AddOrUpdate(category);
        }

        /// <summary>
        /// Add Or Update Category, Add it if it doesn't exist else Update it.
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        private ActionResult AddOrUpdate(CategoryAddDTO category)
        {
            CategoryReadDTO oldCategory = categoryService.GetCategory(category.Id);

            // Add...
            if (oldCategory == null)
            {
                CategoryReadDTO createdCategory = categoryService.Add(category);
                return Created(createdCategory.Name, createdCategory);
            }

            // Update...
            oldCategory.Name = category.Name;
            categoryService.Update(oldCategory);
            return Ok(oldCategory);
        }
    }
}
