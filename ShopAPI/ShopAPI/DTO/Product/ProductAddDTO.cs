﻿using System.ComponentModel.DataAnnotations;

namespace ShopAPI.DTO.Product
{
    public class ProductAddDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "Id is required.")]
        public int Id { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
