﻿using ShopAPI.DTO.Product;

namespace ShopAPI.DTO
{
    public class ProductPage
    {
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public ProductReadDTO[] Products { get; set; }
    }
}
