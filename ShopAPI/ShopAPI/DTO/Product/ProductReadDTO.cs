﻿using ShopAPI.DTO.Category;
using ShopAPI.Models;

namespace ShopAPI.DTO.Product
{
    public class ProductReadDTO
    {
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public string Name { get; set; }

        public DateTime CreateDate { get; set; }

        public virtual CategoryReadDTO Category { get; set; } = null!;
    }
}
