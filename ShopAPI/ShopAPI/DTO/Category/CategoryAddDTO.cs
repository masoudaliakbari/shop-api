﻿using System.ComponentModel.DataAnnotations;

namespace ShopAPI.DTO.Category
{
    public class CategoryAddDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "Id is required.")]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
