﻿using ShopAPI.Models;

namespace ShopAPI.DTO.Category
{
    public class CategoryReadDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
