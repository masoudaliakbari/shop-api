﻿namespace ShopAPI.DTO.Category
{
    public class CategoryPage
    {
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public CategoryReadDTO[] Categories { get; set; }
    }
}
