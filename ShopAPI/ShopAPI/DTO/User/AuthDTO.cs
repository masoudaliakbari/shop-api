﻿using System.ComponentModel.DataAnnotations;

namespace ShopAPI.DTO.User
{
    public class AuthDTO
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
