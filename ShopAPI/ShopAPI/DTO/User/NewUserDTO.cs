﻿using ShopAPI.Models;
using System.ComponentModel.DataAnnotations;

namespace ShopAPI.DTO.User
{
    public class NewUserDTO
    {
        [Required]
        public int RoleId { get; set; }
        [Required]
        public string FirstName { get; set; } = null!;
        [Required]
        public string? LastName { get; set; }
        [Required]
        public string UserName { get; set; } = null!;
        [Required]
        public string Password { get; set; }
    }
}
