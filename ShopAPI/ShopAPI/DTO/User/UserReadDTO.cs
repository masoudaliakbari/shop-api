﻿using ShopAPI.Models;

namespace ShopAPI.DTO
{
    public class UserReadDTO
    {
        public int Id { get; set; }

        public int RoleId { get; set; }

        public string FirstName { get; set; } = null!;

        public string? LastName { get; set; }

        public string UserName { get; set; } = null!;

        public bool? Enable { get; set; }
    }
}
